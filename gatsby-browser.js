/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

import "firebase/analytics"

export function onServiceWorkerUpdateReady() {
  const answer = window.confirm(
    "Esta aplicación ha recibido una actualización.\n"
    + "Desea actualizar a la última versión?"
  )

  if (answer) {
    window.location.reload()
  }
}
