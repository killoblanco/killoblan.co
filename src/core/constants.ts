export const ROUTE_PATHS = {
  LANDING: '/',
  EXPERIENCE: '/experience/',
  BLOG: '/blog/',
  APPS_N_COMPONENTS: '/apps-and-components/',
  PODCAST: '/podcast/',
  YOUTUBE: '/youtube/',
}