import Typography from "typography"

const typography = new Typography({
  headerFontFamily: ["Montserrat", "sans-serif"],
  bodyFontFamily: ["Montserrat", "sans-serif"],
  bodyWeight: '500',
  googleFonts: [{
    name: "Montserrat",
    styles: ["100", "200", "300", "400", "500", "600", "700", "800", "900"]
  },{
    name: "Ubuntu Mono",
    styles: ["100", "200", "300", "400", "500", "600", "700", "800", "900"]
  }]
})

export const { scale, rhythm, options } = typography
export default typography
