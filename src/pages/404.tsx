import React from "react"
import SEO from "../components/seo"
import BaseLayout from "../layouts/base"

const NotFoundPage = () => (
  <BaseLayout>
    <div>
      <SEO title="404: Not found" />
      <h1>NOT FOUND</h1>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    </div>
  </BaseLayout>
)

export default NotFoundPage
