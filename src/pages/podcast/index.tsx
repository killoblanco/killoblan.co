import { PageProps } from "gatsby"
import * as React from "react"
import BaseLayout from "../../layouts/base"

function PodcastPage({ path }: PageProps) {
  return (
    <BaseLayout path={path} color="purple">
      <h1>Koding con K Podcast</h1>
    </BaseLayout>
  )
}

export default PodcastPage
