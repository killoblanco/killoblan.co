import { PageProps } from "gatsby"
import * as React from "react"
import BaseLayout from "../../layouts/base"

function AppsAndComponentsPage({ path }: PageProps) {
  return (
    <BaseLayout path={path}>
      <h1>Applicaciones y Componentes</h1>
    </BaseLayout>
  )
}

export default AppsAndComponentsPage
