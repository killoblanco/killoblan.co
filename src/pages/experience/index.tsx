import { PageProps } from "gatsby"
import * as React from "react"
import ExperienceItem from "../../components/experienceItem"
import BaseLayout from "../../layouts/base"
import SEO from "../../components/seo"
import styles from "./experience-page.module.scss"

function ExperiencePage({ path }: PageProps) {
  return (
    <BaseLayout path={path}>
      <SEO title="Experience" />
      <div className={styles.container}>
        <h1>Experiencia</h1>
        <h3>Globant</h3>
        <ExperienceItem
          buildTo="Stanley Black & Decker"
          date="Ago 2019 - Presente"
          name="Tool Connect Site Manager"
        />
        <h2>2019</h2>
        <h3>Globant</h3>
        <ExperienceItem
          buildTo="Quantcast"
          date="Feb 2019 - Ago 2019"
          name="Cookie Settings Manager"
        />
        <ExperienceItem
          buildTo="Banco Davivienda"
          date="Ago 2018 - Ene 2019"
          name="Tiendirri - PWA"
        />
        <h2>2018</h2>
        <h3>Soft Dev Team</h3>
        <ExperienceItem
          date="Ago 2017 - Jul 2018"
          buildTo="Docuflux"
        />
        <h2>2017</h2>
        <h3>Optime Consulting</h3>
        <ExperienceItem
          date="Abr 2015 - May 2017"
          buildTo="Diferentes proyectos"
        />
        <h2>2015</h2>
        <h3>Loma Management Ltd</h3>
        <ExperienceItem
          date="Dic 2012 - Mar 2015"
          buildTo="Sitio Web"
        />
      </div>
    </BaseLayout>
  )
}

export default ExperiencePage