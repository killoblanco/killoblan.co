import { PageProps } from "gatsby"
import * as React from "react"
import BaseLayout from "../../layouts/base"

function YouTubePage({ path }: PageProps) {
  return (
    <BaseLayout path={path} color="red">
      <h1>Koding con K TV</h1>
    </BaseLayout>
  )
}

export default YouTubePage
