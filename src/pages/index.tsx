import { mdiEmailOutline, mdiGithub, mdiLinkedin } from "@mdi/js"
import Icon from "@mdi/react"
import { PageProps } from "gatsby"
import * as React from "react"
import SEO from "../components/seo"
import BaseLayout from "../layouts/base"
import styles from "./index.module.scss"

function IndexPage({ path }: PageProps) {
  const mediaLinks = [
    { key: "github", href: "https://github.com/killoblanco", icon: mdiGithub },
    {
      key: "linkedin",
      href: "https://www.linkedin.com/in/killoblanco/",
      icon: mdiLinkedin
    },
    {
      key: "email",
      href: "mailto:killo.blanco@gmail.com?subject=Hola%20Kamilo",
      icon: mdiEmailOutline
    }
  ]

  return (
    <BaseLayout path={path}>
      <SEO title="Home" />
      <article className={styles.container}>
        <h1>Hola, Soy Kamilo</h1>
        <p>
          Conectado profesionalmente con tecnologías de vanguardia en el
          desarrollo de software multiplataforma y tecnologías de la información
          durante muchos años.
        </p>
        <p>
          Una persona bien organizada, solucionador de problemas, trabajo en
          equipo, mentor, arquitecto de software y desarrollador full-stack.
        </p>
        <nav>
          {mediaLinks.map(media => (
            <a href={media.href} key={media.key}>
              <Icon path={media.icon} />
            </a>
          ))}
        </nav>
      </article>
    </BaseLayout>
  )
}

export default IndexPage
