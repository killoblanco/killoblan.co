import clsx from 'clsx'
import * as React from 'react'
import styles from './experienceItem.module.scss'

interface ExperienceItemProps {
  buildTo: string,
  date: string,
  name?: string,
}

function ExperienceItem({ buildTo, date, name }: ExperienceItemProps) {
  return (
    <div className={clsx(styles.container, { [styles.workInfoSingle]: !name })}>
      {name ? (<p className={styles.name}>{name}</p>) : null}
      <p className={styles.buildTo}>{buildTo}</p>
      <p className={styles.date}>{date}</p>
    </div>
  )
}

export default ExperienceItem
