import {
  mdiContactsOutline,
  mdiPostOutline,
  mdiTimelineTextOutline
} from "@mdi/js"
import Icon from "@mdi/react"
import * as React from "react"
import { ROUTE_PATHS } from "../../core/constants"
import Link from "../link"
import styles from "./sidebar.module.scss"

function Sidebar({ path }: { path: string }) {
  const linkList = [
    { to: ROUTE_PATHS.LANDING, icon: mdiContactsOutline },
    { to: ROUTE_PATHS.EXPERIENCE, icon: mdiTimelineTextOutline },
    { to: ROUTE_PATHS.BLOG, icon: mdiPostOutline }
    // { to: ROUTE_PATHS.APPS_N_COMPONENTS, icon: mdiApps },
    // { to: ROUTE_PATHS.PODCAST, icon: mdiGooglePodcast },
    // { to: ROUTE_PATHS.YOUTUBE, icon: mdiYoutubeTv }
  ]

  return (
    <div className={styles.container}>
      {linkList.map(link =>
        link.to === path
          ? <Icon path={link.icon} key={link.to} />
          : (
            <Link to={link.to} key={link.to}>
              <Icon path={link.icon} />
            </Link>
          ))}
    </div>
  )
}

export default Sidebar
