import AniLink from "gatsby-plugin-transition-link/AniLink"
import * as React from "react"
import { PropsWithChildren } from "react"

interface LinkProps {
  to: string
}

function Link({ children, to }: PropsWithChildren<LinkProps>) {
  return (
    <AniLink paintDrip to={to} hex="#00bfa5" duration={0.3}>
      {children}
    </AniLink>
  )
}

export default Link
