import { graphql, PageProps } from "gatsby"
import * as React from "react"
import SEO from "../components/seo"
import BaseLayout from "../layouts/base"
import { BlogPageProps } from "./blog-types"
import styles from "./blog.module.scss"
import PostsList from "./components/list"

function BlogPage({ path, data }: PageProps<BlogPageProps>) {
  return (
    <BaseLayout path={path} color="white">
      <SEO title="Koding con K Blog" />
      <div className={styles.container}>
        <h1>Koding con K Blog</h1>
        <PostsList
          data={data.allMarkdownRemark.edges}
        />
      </div>
    </BaseLayout>
  )
}

export default BlogPage

export const query = graphql`
  query ($limit: Int!, $skip: Int!) {
    allMarkdownRemark(
      filter: { frontmatter: { published: { eq: true } } },
      sort: { fields: frontmatter___date, order: DESC },
      limit: $limit,
      skip: $skip
    ) {
      edges {
        node {
          fields {
            slug
          }
          frontmatter {
            title
            tags
            date(formatString: "MMM DD, YYYY", locale: "es")
            category
            published
          }
          timeToRead
          excerpt(format: PLAIN)
        }
      }
      pageInfo {
        currentPage
        hasNextPage
        hasPreviousPage
        itemCount
        pageCount
        perPage
        totalCount
      }
    }
  }
`
