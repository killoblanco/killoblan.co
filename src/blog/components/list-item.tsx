import * as React from "react"
import Link from "../../components/link"
import { PostListItemProps } from "../blog-types"
import styles from "../blog.module.scss"

function PostsListItem({ post }: PostListItemProps) {
  return (
    <div className={styles.postListItem}>
      <Link to={post.fields.slug}>
        <h2>{post.frontmatter.title}</h2>
        <p>{post.excerpt}</p>
      </Link>
      <p>
        {post.frontmatter.date}
        &nbsp;&nbsp;·&nbsp;&nbsp;
        {post.timeToRead} minuto{!!post.timeToRead ? "s" : ""}
      </p>
    </div>
  )
}

export default PostsListItem
