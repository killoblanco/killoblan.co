import * as React from "react"
import { PostListProps } from "../blog-types"
import styles from "../blog.module.scss"
import PostsListItem from "./list-item"

function PostsList({ data }: PostListProps) {
  return (
    <div className={styles.postList}>
      {data.map(({ node }) => (
        <PostsListItem post={node} key={node.fields.slug} />
      ))}
    </div>
  )
}

export default PostsList
