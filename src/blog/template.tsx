import { defineCustomElements } from "@deckdeckgo/highlight-code/dist/loader"
import { DiscussionEmbed } from "disqus-react"
import { graphql, PageProps } from "gatsby"
import * as React from "react"
import BaseLayout from "../layouts/base"
import { PostItemProps } from "./blog-types"
import styles from "./blog.module.scss"

function BlogPostTemplate({ path, data }: PageProps<PostItemProps>) {
  const disqusConfig = {
    shortname: process.env.GATSBY_DISQUS_NAME,
    config: {
      identifier: data.markdownRemark.fields.slug,
      title: data.markdownRemark.frontmatter.title
    }
  }

  React.useEffect(() => {
    defineCustomElements()
  }, [])

  console.log(data.markdownRemark.fields.slug)

  return (
    <BaseLayout path={path} color="white">
      <article className={styles.postContainer}>
        <h1>{data.markdownRemark.frontmatter.title}</h1>
        <p>
          {data.markdownRemark.frontmatter.date}
          &nbsp;&nbsp;·&nbsp;&nbsp;
          {data.markdownRemark.timeToRead} minutos
        </p>
        <div dangerouslySetInnerHTML={{ __html: data.markdownRemark.html }} />
        <DiscussionEmbed shortname="killoblanco" config={disqusConfig.config} />
      </article>
    </BaseLayout>
  )
}

export default BlogPostTemplate

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      fields {
        slug
      }
      frontmatter {
        title
        tags
        date(formatString: "MMM DD, YYYY", locale: "es")
        category
      }
      timeToRead
    }
  }
`
