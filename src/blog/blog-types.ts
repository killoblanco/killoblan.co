export interface BlogEdgeNode {
  excerpt: string
  timeToRead: number
  html: string
  fields: {
    slug: string
  }
  frontmatter: {
    tags: string[]
    category: string[]
    date: string
    published: string
    title: string
  }
}

export type BlogEdges = { node: BlogEdgeNode }[]

export interface BlogPageInfo {
  currentPage: number
  hasNextPage: boolean
  hasPreviousPage: boolean
  itemCount: number
  pageCount: number
  perPage: number
  totalCount: number
}

export interface BlogPageProps {
  allMarkdownRemark: {
    edges: BlogEdges
    pageInfo: BlogPageInfo
  }
}

export interface PostListProps {
  data: BlogEdges
}

export interface PostListItemProps {
  post: BlogEdgeNode
}

export interface PostItemProps {
  markdownRemark: BlogEdgeNode
}
