import clsx from "clsx"
import firebase from "gatsby-plugin-firebase"
import * as React from "react"
import Sidebar from "../../components/sidebar"

import styles from "./base-layout.module.scss"

interface BaseLayoutProps {
  path: string,
  color?: "default" | "white" | "purple" | "red"
}

function BaseLayout({ children, path, color }: React.PropsWithChildren<BaseLayoutProps>) {
  const [bgColor, setBgColor] = React.useState({})

  React.useEffect(() => {
    if (process.env.NODE_ENV !== "development") {
      firebase.analytics()
    }
    setBgColor({
      [styles.fadeToWhite]: color === "white",
      [styles.fadeToPurple]: color === "purple",
      [styles.fadeToRed]: color === "red"
    })
  }, [])

  return (
    <div
      className={clsx(styles.container, bgColor)}
    >
      <div className={styles.menu}>
        <Sidebar path={path} />
      </div>
      <div className={styles.content}>
        {children}
      </div>
      <footer className={styles.copy}>
        &copy;&nbsp;{new Date().getFullYear()}
      </footer>
    </div>
  )
}

BaseLayout.defaultProps = {
  color: "default"
}

export default BaseLayout
