require('dotenv').config({
  path: `.env`
})

const siteMetadata = {
  title: `Killoblan.co`,
  description: `Conectado profesionalmente con tecnologías de vanguardia en el desarrollo de software multiplataforma y tecnologías de la información durante muchos años.\n\nUna persona bien organizada, solucionador de problemas, trabajo en equipo, mentor, arquitecto de software y desarrollador full-stack.`,
  author: `@killoblanco`,
  siteUrl: `https://killoblan.co`
}

const plugins = [
  `gatsby-plugin-react-helmet`,
  {
    resolve: `gatsby-source-filesystem`,
    options: {
      name: `images`,
      path: `${__dirname}/src/images`
    }
  },
  {
    resolve: `gatsby-source-filesystem`,
    options: {
      name: `blog`,
      path: `${__dirname}/src/blog`
    }
  },
  {
    resolve: `gatsby-transformer-remark`,
    options: {
      plugins: [
        {
          resolve: `gatsby-remark-highlight-code`,
          options: {
            theme: `oceanic-next`
          }
        }
      ]
    }
  },
  `gatsby-transformer-sharp`,
  `gatsby-plugin-sharp`,
  {
    resolve: `gatsby-plugin-manifest`,
    options: {
      name: `Killoblan.co`,
      short_name: `Killoblan.co`,
      start_url: `/`,
      background_color: `#009688`,
      theme_color: `#009688`,
      display: `standalone`,
      icon: `src/images/thumbnail.png` // This path is relative to the root of the site.
    }
  },
  `gatsby-plugin-sass`,
  {
    resolve: `gatsby-plugin-typography`,
    options: {
      pathToConfigModule: `src/core/typography`
    }
  },
  `gatsby-plugin-offline`,
  `gatsby-plugin-transition-link`,
  {
    resolve: `gatsby-plugin-firebase`,
    options: {
      credentials: {
        apiKey: "AIzaSyAzaIchDOdQk35NWJ9AFbo_pcELpU3rq3E",
        authDomain: "killoblanco-61e6e.firebaseapp.com",
        databaseURL: "https://killoblanco-61e6e.firebaseio.com",
        projectId: "killoblanco-61e6e",
        storageBucket: "killoblanco-61e6e.appspot.com",
        messagingSenderId: "241019596580",
        appId: "1:241019596580:web:32a7692f0b752a09",
        measurementId: "G-Y4JK5T6F79"
      }
    }
  },
  `gatsby-plugin-sitemap`
]

module.exports = {
  siteMetadata,
  plugins
}
